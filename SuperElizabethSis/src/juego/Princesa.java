package juego;
import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Princesa 
{
	private int x;
	private int y;
	private int alto;
	private int ancho;
	private int velocidadX;
	private int velocidadY;
	
	Princesa(int x,int y,int alto,int ancho)
	{
		this.x= x;
		this.y= y;
		this.alto= alto;
		this.ancho= ancho;
		this.velocidadX= 2;
		this.velocidadY= 10;
		
	}
	public void moverDerecha() 
	{ 
		this.x= this.x + velocidadX;
	}
	public void moverIzquierda() 
	{
		this.x= this.x - velocidadX;
	}
	public void subir() 
	{
		this.y= this.y - velocidadY;
	}
	public void bajar() 
	{
		this.y= this.y + velocidadY;
	}                                    
	public void dibujar(Entorno e)
	{
		
		Herramientas herramienta= new Herramientas();
		Image imagen= herramienta.cargarImagen("Images\\photP2.png");
		e.dibujarImagen(imagen, this.x, this.y-20, 0, 0.7); 
	}
	public BolaDeFuego Disparar() 
	{
		BolaDeFuego bola= new BolaDeFuego(this.x, this.y, 20);
		return bola;
	}
	public int getX() 
	{
		return x;
	}
	public int getY() 
	{
		return y;
	}
	public int getAlto() 
	{
		return alto;
	}
	public int getAncho() 
	{
		return ancho;
	}
	public int getVelocidadX() 
	{
		return velocidadX;
	}
	

	
}


