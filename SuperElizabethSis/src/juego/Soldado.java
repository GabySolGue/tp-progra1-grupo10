package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Soldado {

	private int x;
	private int y;
	private int alto;
	private int ancho;
	private int velocidad;
	
	public Soldado (int x, int y, int alto, int ancho) 
	{
		this.x = x;
		this.y= y;
		this.alto= alto;
		this.ancho= ancho;
		this.velocidad=2;
	}
	
	public void dibujar(Entorno e) 
	{
		
		
		Herramientas herramienta= new Herramientas();
		Image imagen= herramienta.cargarImagen("Images\\Soldado1.png");
		e.dibujarImagen(imagen, this.x, this.y-15, 0, 0.4);			
	}
	public void mover() 
	{
		this.x= this.x - velocidad;
		
	}
	public int getX() 
	{
		return x;
	}
	public int getY() 
	{
		return y;
	}
	public int getAlto() 
	{
		return alto;
	}
	public int getAncho() 
	{
		return ancho;
	}
	public void setX(int x) 
	{
		this.x = x;
	}

	public int getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}	
	
}

