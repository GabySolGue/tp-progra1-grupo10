package juego;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.util.Random;
import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Juego extends InterfaceJuego
{
	private Entorno entorno;
	private Princesa prince;
	private Obstaculo[] obstaculos; 
	private Soldado[] arrSoldados;
	private BolaDeFuego[] bolasDeFuego;
	private boolean saltando;
	private boolean bajando;
	private boolean jugar;
	private int vidas;
	private int puntos; 
	private double ticks;
	private boolean estaColisionandoObj;
	private boolean estaColisionandoSol;
	
	Juego()
	{
		//Iniciamos objetos
		this.entorno = new Entorno(this, "Super Elizabeth Sis - Grupo 10(Guerrero, Jacob y Saltiva) - v1", 800, 600);
		this.prince= new Princesa(entorno.ancho()/5,entorno.alto()-100,55,30);
		Herramientas he= new Herramientas();
		Clip p= he.cargarSonido("Sonidos\\Mfondo.wav");
		he.play("Sonidos\\Mfondo.wav");
	
		
		//Creamos los obstaculos	
		this.obstaculos= new Obstaculo[2];
		obstaculos[0]= new Obstaculo(entorno.ancho(), entorno.alto()-100, 55, 30);
		obstaculos[1]= new Obstaculo(entorno.ancho()+400, entorno.alto()-100, 55, 30);
		
		//Creamos arreglo de dos Soldados
		this.arrSoldados= new Soldado[2];
		arrSoldados[0]= new Soldado(entorno.ancho(), entorno.alto()-100, 55, 30);
		arrSoldados[1]= new Soldado(entorno.ancho()+200, entorno.alto()-100, 55, 30);
	
		this.bolasDeFuego= new BolaDeFuego[2];
		this.saltando= false;
		this.bajando= false;
		this.jugar= true;
		this.vidas= 3;
		this.puntos= 0;
		this.ticks= 0;
		this.estaColisionandoObj=false;
		this.estaColisionandoSol=false;
		
		// Inicia el juego!
		this.entorno.iniciar();
	}
	
	public void tick()
	{
		
//FONDO		
		Herramientas herramienta= new Herramientas();
		Image imagen= herramienta.cargarImagen("Images\\fondoC.png");
		this.entorno.dibujarImagen(imagen, 400, 300, 0, 1);
		
//VIDAS Y PUNTOS EN PANTALLA		
		entorno.cambiarFont("ITALIC", 20, Color.black);
		String msjVida= "VIDAS: " + vidas; 
		entorno.escribirTexto(msjVida, entorno.ancho()/3, 50);
		
		String msjPuntos= "PUNTOS: "+ puntos;
		entorno.escribirTexto(msjPuntos, entorno.ancho()/2, 50);
		


//COMIENZO DEL JUEGO
		if (vidas==0) 
		{
			
			jugar= false;
			
		}
		if (jugar==true) { 
	
			//DIBUJO DE OBJETOS
			prince.dibujar(this.entorno);
			
			for (int i= 0; i < obstaculos.length; i++) 
			{
				obstaculos[i].dibujar(entorno);
			}
			for (int i=0; i<arrSoldados.length;i++) 
			{
				if (arrSoldados[i]!= null) 
				{
					arrSoldados[i].dibujar(entorno);
				}
			}
			//MOVIMIENTO DE OBSTACULOS
			ticks++;
			int num1=50;
	        int num2=120;
	        int numAleatorio= (int)Math.floor(Math.random()*(num1-num2)+num2);
	        
			movimientoObstaculos (obstaculos, entorno);
			movimientoSoldados (arrSoldados, entorno, ticks, numAleatorio);
			
			//CREACION Y MOVIMIENTO DE BOLA DE FUEGO
			if(this.entorno.sePresiono(this.entorno.TECLA_ESPACIO)) 
			{
				Herramientas h= new Herramientas();
				Clip s= h.cargarSonido("Sonidos\\Magia.wav");
				h.play("Sonidos\\Magia.wav");
				int i= 0;
				while (i < 2 && bolasDeFuego[i] != null) 
				{
					i++;
				}
				if (i < 2 && prince.getY() > this.entorno.alto()-200) 
				{
					bolasDeFuego[i]= prince.Disparar();
				
				}
			}
			for (int b = 0; b < this.bolasDeFuego.length; b++) 
			{
				if (this.bolasDeFuego[b] != null && this.bolasDeFuego[b].getX() <= entorno.ancho()) 
				{
					this.bolasDeFuego[b].mover();
					this.bolasDeFuego[b].dibujar(entorno);
				}
				else if (this.bolasDeFuego[b] != null) 
				{
					this.bolasDeFuego[b] = null;
				}
			}
		
			
			
			//MOVIMIENTOS DE PRINCESA
			if(this.entorno.estaPresionada(this.entorno.TECLA_DERECHA) && prince.getX() < this.entorno.ancho()/2) 
			{
				prince.moverDerecha();
			}
			if(this.entorno.estaPresionada(this.entorno.TECLA_IZQUIERDA) && prince.getX() > 20) 
			{
				prince.moverIzquierda();
			}
			if (this.prince.getY() == 500 && this.entorno.sePresiono(this.entorno.TECLA_ARRIBA)) 
			{
				saltando= true;
			}
			if (saltando == true && prince.getY() > 250) 
			{
				prince.subir();
				Herramientas herr= new Herramientas();
				Clip son= herr.cargarSonido("Sonidos\\Salto.wav");
				herr.play("Sonidos\\Salto.wav");
			} 
			else if (saltando == true) 
			{
				bajando= true;
				saltando = false;
				prince.bajar();
			} 
			else if (bajando == true && prince.getY() > 250 && prince.getY() < 500 ) 
			{
				prince.bajar();
			} 
			else 
			{
				saltando= false;
				bajando= false;
			}
			
		
			//COLISIONES
			
			
			
			if (colisionPrincesaSoldado()== true && estaColisionandoSol==false) 
			{
				vidas= vidas-1;
				estaColisionandoSol=true;
			}
			if (colisionPrincesaObstaculo()== true  && estaColisionandoObj==false) 
			{
				vidas= vidas-1;
				estaColisionandoObj=true;
			}
			

			if (!colisionPrincesaSoldado() && estaColisionandoSol==true) 
			{
				
				estaColisionandoSol=false;
			}
			if (!colisionPrincesaObstaculo()  && estaColisionandoObj==true) 
			{
				
				estaColisionandoObj=false;
			}
			
			
		
			for (int i = 0; i < arrSoldados.length; i++) 
			{	
				for (int j = 0; j < bolasDeFuego.length; j++) 
				{
					if(bolasDeFuego[j] != null && arrSoldados[i] != null) 
					{
						if (colisionBolaSoldado(arrSoldados[i], bolasDeFuego[j]))
						{
							puntos = puntos + 5;
							arrSoldados[i] = null;
							bolasDeFuego[j] = null;
						}
					}
				}
			}	
	
		}
		else 
		{ //jugar sea false
			
			entorno.cambiarFont("ITALIC", 100, Color.red);
			String mensaje = "GAME OVER";
			entorno.escribirTexto(mensaje, entorno.ancho()/6, entorno.alto()-entorno.alto()/3);
			
		}
	
	}

	@SuppressWarnings("unused")
	public static void main(String[] args)
	{
		Juego juego = new Juego();
	}

//METODOS DE CLASE
	
	public static void movimientoObstaculos (Obstaculo[] o, Entorno e) 
	{
		for (int i= 0; i < o.length; i++) 
		{
			if (o[i].getX() >= 0) 
			{
				o[i].mover();
			} 
			else if (o[i].getX() < 0) 
			{
				o[i].setX(e.ancho());
			}
		}
	}
	
	public static void movimientoSoldados (Soldado[] s, Entorno e, double ticks, int aleatorio) 
	{
		for (int i= 0; i < s.length; i++) 
		{
			if (s[i] != null && s[i].getX() >= 0) 
			{
				s[i].mover();
			} 
			else if (s[i] != null && s[i].getX() < 0) 
			{
				s[i]= null;
			}
		}
		for (int i= 0; i< s.length; i++) 
		{
			if (s[i] == null) 
			{
				if (ticks%aleatorio == 0) {
					s[i]= new Soldado(e.ancho(), 500, 55,30);
				}
			}
		}
	}
	
	
	//Colisiones para verificar si hay contacto con la princesa
	public boolean colisionPrincesaSoldado() 
	{
		for (int i= 0; i < arrSoldados.length; i++) 
		{
			if(arrSoldados[i] != null) 
			{
				int princeLadoSup= this.prince.getY() - this.prince.getAlto()/2;
				int princeLadoInf= this.prince.getY() + this.prince.getAlto()/2;
				int princeLadoDer= this.prince.getX() + this.prince.getAncho()/2;
				int princeLadoIzq= this.prince.getX() - this.prince.getAncho()/2;
				int soldLadoSup= this.arrSoldados[i].getY() - this.arrSoldados[i].getAlto()/2;
				int soldLadoInf= this.arrSoldados[i].getY() + this.arrSoldados[i].getAlto()/2;
				int soldLadoIzq= this.arrSoldados[i].getX() - this.arrSoldados[i].getAncho()/2;
				int soldLadoDer= this.arrSoldados[i].getX() + this.arrSoldados[i].getAncho()/2;
				
				boolean colisionFrontal= princeLadoDer >= soldLadoIzq && princeLadoSup == soldLadoSup && princeLadoInf == soldLadoInf && princeLadoIzq <= soldLadoDer;
				boolean colisionParcialX= princeLadoDer >= soldLadoIzq && princeLadoInf <= soldLadoInf && princeLadoInf >= soldLadoSup && princeLadoIzq <= soldLadoDer;
				boolean colisionParcialY= princeLadoInf == soldLadoSup && princeLadoIzq >= soldLadoIzq && princeLadoIzq <= soldLadoDer || princeLadoInf == soldLadoSup && princeLadoDer >= soldLadoIzq && princeLadoDer <= soldLadoDer;
				
				if (colisionFrontal || colisionParcialX || colisionParcialY) 
				{
					return true;
				}
			}
		}
		return false;
	}
	
	//Verifica si hay contacto con los obstaculos
	
	public boolean colisionPrincesaObstaculo() 
	{
		for (int i= 0; i < obstaculos.length; i++) 
		{
			int princeLadoSup= this.prince.getY() - this.prince.getAlto()/2;
			int princeLadoInf= this.prince.getY() + this.prince.getAlto()/2;
			int princeLadoDer= this.prince.getX() + this.prince.getAncho()/2;
			int princeLadoIzq= this.prince.getX() - this.prince.getAncho()/2;
			int obsLadoSup= this.obstaculos[i].getY() - this.obstaculos[i].getAlto()/2;
			int obsLadoInf= this.obstaculos[i].getY() + this.obstaculos[i].getAlto()/2;
			int obsLadoIzq= this.obstaculos[i].getX() - this.obstaculos[i].getAncho()/2;
			int obsLadoDer= this.obstaculos[i].getX() + this.obstaculos[i].getAncho()/2;
			
			boolean colisionFrontal= princeLadoDer >= obsLadoIzq && princeLadoSup == obsLadoSup && princeLadoInf == obsLadoInf && princeLadoIzq <= obsLadoDer;
			boolean colisionParcialX= princeLadoDer >= obsLadoIzq && princeLadoInf <= obsLadoInf && princeLadoInf >= obsLadoSup && princeLadoIzq <= obsLadoDer;
			boolean colisionParcialY= princeLadoInf == obsLadoSup && princeLadoIzq >= obsLadoIzq && princeLadoIzq <= obsLadoDer || princeLadoInf == obsLadoSup && princeLadoDer >= obsLadoIzq && princeLadoDer <= obsLadoDer;
			
			if (colisionFrontal || colisionParcialX || colisionParcialY) 
			{
				Herramientas herra= new Herramientas();
				Clip sonido= herra.cargarSonido("Sonidos\\Colision.wav");
				herra.play("Sonidos\\Colision.wav");
				return true;
			}
		}
		return false;
	}
	
	//Verifica si hay colision entre la bola de fuego y los soldados
	public boolean colisionBolaSoldado(Soldado s, BolaDeFuego b) 
	{ 
		boolean c1 = b.getX() + b.getDiametro() / 2 >= s.getX() - s.getAncho() / 2; //borde derecho de bola <= borde izq de soldado
		boolean c2 = b.getX() + b.getDiametro() / 2 <= s.getX() + s.getAncho() / 2; //borde derecho de bola <= borde derecho del soldado
		boolean c3 = b.getY() + b.getDiametro() / 2 >= s.getY() - s.getAlto() / 2;	// borde inferior de bola >= borde superior de soldado
		
		return c1 && c2 && c3;				
	}
}
