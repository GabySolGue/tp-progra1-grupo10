package juego;


import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class BolaDeFuego {
	
	private int x;
	private int y;
	private int diametro;
	private int velocidad; 
	
	public BolaDeFuego(int x, int y, int diametro) //Constructor
	{
		this.x= x;
		this.y= y;
		this.diametro= diametro;
		this.velocidad= 4;
	}
	public void dibujar(Entorno e) //Dibuja a la bola de fuego
	{
		
		Herramientas herramienta= new Herramientas();
		Image imagen= herramienta.cargarImagen("Images\\bola.png");
		e.dibujarImagen(imagen, this.x, this.y-20, 0, 0.1); 
	}
	public void mover() 
	{
		this.x= this.x + velocidad;
	}
	public int getX() 
	{
		return x;
	}
	public int getDiametro() 
	{
		return diametro;
	}
	public int getY() 
	{
		return y;
	}
	
}
